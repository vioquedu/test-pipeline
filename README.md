# Test Pipelines

A dummy project to illustrate how to run Gitlab pipelines.

## Table of contents

1. How to create a Gitlab pipeline.
2. How to store environment variables in Gitlab.
3. Saving test coverage.
4. Reporting test coverage in a Gitlab badge.

